#Dekomentarisemo donji kod u slucaju da postoji bp sa imenom biblioteka na serveru
#DROP DATABASE IF EXISTS biblioteka;

# Pravimo bazu podataka pod uslovom da vec ne postoji
CREATE DATABASE IF NOT EXISTS biblioteka;
USE biblioteka;

# Pravimo tabele u bazi podataka pod uslovom da vec ne postoje
CREATE TABLE IF NOT EXISTS knjige(
KnjigaID int auto_increment primary key,
Naslov varchar(45) not null,
Autor varchar(45),
BrojStrana int not null,
Deskripcija varchar(250) not null) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS clanovi(
ClanID int auto_increment primary key,
Ime varchar(45) not null,
Prezime varchar(45) not null,
BrojClanskeKarte int not null)ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS pozajmice(
PozajmicaID int auto_increment primary key,
Clan int not null,
Knjiga int not null,
FOREIGN KEY (Clan) REFERENCES clanovi(ClanID),
FOREIGN KEY (Knjiga) REFERENCES knjige(KnjigaID),
Datum DATE not null)ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS rezervacije(
RezervacijaID int auto_increment primary key,
Clan int not null,
Knjiga int not null,
FOREIGN KEY (Clan) REFERENCES clanovi(ClanID),
FOREIGN KEY (Knjiga) REFERENCES knjige(KnjigaID),
Uzeto DATE not null,
Vraceno DATE not null)ENGINE = InnoDB;
