﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace biblioteka
{
    public partial class Form2 : Form
    {
        MySqlConnection veza = new MySqlConnection("datasource = localhost; port = 3306; username = root; password = root");
        MySqlCommand komanda;
        MySqlDataAdapter adapter;
        DataTable tabla;

        Int32 ID;

        public Form2()
        {
            InitializeComponent();
            loadDataGrid();
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_return_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 form_meni = new Form1();
            form_meni.Show();
        }

        private void loadDataGrid()
        {
            veza.Open();
            string select_query = "SELECT * FROM biblioteka.clanovi";

            adapter = new MySqlDataAdapter();
            komanda = new MySqlCommand(select_query, veza);
            adapter.SelectCommand = komanda;

            tabla = new DataTable();
            adapter.Fill(tabla);

            BindingSource bs = new BindingSource();
            bs.DataSource = tabla;

            dataGridView1.DataSource = bs;
            veza.Close();
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            string query_update = "UPDATE biblioteka.clanovi SET Ime = '" + textBoxIme.Text + "', Prezime = '" + textBoxPrezime.Text + "', BrojClanskeKarte = '" + textBoxBrojClanskeKarte.Text + "' WHERE ClanID IN ('" + ID + "')";
            komanda = new MySqlCommand(query_update, veza);

            veza.Open();
            komanda.ExecuteNonQuery();
            veza.Close();
            loadDataGrid(); // Refreshujemo prikaz tabele
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            string query_insert = "INSERT INTO biblioteka.clanovi(Ime, Prezime, BrojClanskekarte) VALUES ('" + this.textBoxIme.Text + "', '" + this.textBoxPrezime.Text + "', '" + this.textBoxBrojClanskeKarte.Text + "')";
            komanda = new MySqlCommand(query_insert, veza);

            veza.Open();
            komanda.ExecuteNonQuery();
            veza.Close();
            loadDataGrid(); // Refreshujemo prikaz tabele
        }

        private void btn_remove_Click(object sender, EventArgs e)
        {
            string query_remove = "DELETE FROM biblioteka.clanovi WHERE ClanID IN ('" + ID + "')";
            komanda = new MySqlCommand(query_remove, veza);

            veza.Open();
            komanda.ExecuteNonQuery();
            veza.Close();
            loadDataGrid();
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            ID = Int32.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            textBoxIme.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            textBoxPrezime.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            textBoxBrojClanskeKarte.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
        }

        void SearchData(string valueToSearch)
        {
            string query_trazi = "SELECT * FROM biblioteka.clanovi WHERE CONCAT (ClanID, Ime, Prezime, BrojClanskeKarte) LIKE '%"+valueToSearch+"%'";
            adapter = new MySqlDataAdapter(query_trazi, veza);
            tabla = new DataTable();
            adapter.Fill(tabla);
            dataGridView1.DataSource = tabla;
        }

        private void textBoxTrazi_TextChanged(object sender, EventArgs e)
        {
            SearchData(textBoxTrazi.Text);
        }

    }
}
