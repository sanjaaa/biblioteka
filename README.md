## About
Biblioteka je aplikacija napravljena u C# koja sluzi za upravljanjem baze podataka biblioteke.

Da bi ste mogli da radite na ovom projektu potrebne su vam sledece stvari:
- Visual Studio 2017
- .NET Framework 4.7.2 SDK
- MySql Paket ili XAMPP(treba vam phpMYAdmin ako uzmete XAMPP)

Projekact je licensiran pod [GNU GPLv3 licensom] (LICENSE.md)

[Kako doprineti-vodic] (CONTRIBUTING.md)